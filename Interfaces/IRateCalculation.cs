﻿using System.Collections.Generic;
using Zopa.Models;


namespace Zopa.Interfaces
{
    public interface IRateCalculation
    {
        QuoteResultModel CalculateRate(int requested, IEnumerable<LenderModel> lenders);
    }
}