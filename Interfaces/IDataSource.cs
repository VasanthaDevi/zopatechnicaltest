﻿using System.Collections.Generic;

namespace Zopa.Interfaces
{
    public interface IDataSource<T>
    {
        List<T> GetAllEntities();
    }
}
