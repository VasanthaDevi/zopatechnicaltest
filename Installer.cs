﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Zopa.Models;
using Zopa.Interfaces;
using Zopa.Services;

namespace Zopa.Installer
{
    public class RepositoriesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDataSource<LenderModel>>().ImplementedBy<LenderDataSource>());
            container.Register(Component.For<IRateCalculation>().ImplementedBy<RateCalculation>());
        }
    }
}
