﻿

namespace Zopa.Models
{
    public class QuoteResultModel
    {
        public int LoanAmount { get; set; }
        public decimal TotalRepayment { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal AnnualRate { get; set; }
    }
}
