﻿

namespace Zopa.Models
{
    public class LenderModel
    {
        public string Lender { get; set; }
        public decimal Rate { get; set; }
        public decimal Available { get; set; }
    }
}
