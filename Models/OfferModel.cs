﻿
namespace Zopa.Models
{
    public class OfferModel
    {
        public int AmountAssigned { get; set; }
        public LenderModel Lender { get; set; }
    }
}