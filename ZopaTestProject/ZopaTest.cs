﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Zopa.Models;
using Zopa.Services;

namespace UnitTestProject1
{
    [TestClass]
    public class ZopaTest
    {


        [TestMethod]
        public void TestLendersList()
        {
            // Arrange
            LenderDataSource obj = new LenderDataSource("MarketData.csv");

            //Act
            var lenderCount = obj.GetAllEntities();

            //Assert
            Assert.AreEqual(7, lenderCount.Count);

        }


        [TestMethod]
        public void TestCalculateRate()
        {
            //Arrange
            var obj = new RateCalculation();
            var lendersList = GetLendersList();

            //Act
            var result = obj.CalculateRate(1000, lendersList);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.AnnualRate, 0.065m);
            Assert.AreEqual(result.LoanAmount, 1000);

        }

        [TestMethod]
        public void SelectOfferesWithLowestIntrestRates()
        {
            //Arrange
            var obj = new RateCalculation();
            var lendersList = GetLendersList();

            //Act
            var result = obj.SelectOfferesWithLowestIntrestRates(1000, lendersList);

            //Assert
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void ValidateLoanAmount()
        {

            //Arrange
            RateCalculation obj = new RateCalculation();

            //Act
            var result = obj.ValidateLoanAmount(1000);

            //Assert
            Assert.AreEqual(result, 1000);



        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Loan amount must be inside the interval of 1000 to 15000")]
        public void TestMaxLoanAmount()
        {
            //Arrange
            var obj = new RateCalculation();
            var lendersList = GetLendersList();

            //Act
            var result = obj.CalculateRate(16000, lendersList);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Loan amount must be dividable by 100.")]
        public void TestLoanAmountStep()
        {
            //Arrange
            var obj = new RateCalculation();
            var lendersList = GetLendersList();

            //Act
            var result = obj.CalculateRate(2150, lendersList);

            //Assert
        }


        [TestMethod]
        [ExpectedException(typeof(Exception), "Market does not have sufficient offers from lenders to satisfy the loan, Requested Loan:14000")]
        public void TestNotSufficientOffers()
        {
            //Arrange
            var obj = new RateCalculation();
            var lendersList = GetLendersList();

            //Act
            var result = obj.CalculateRate(14000, lendersList);

            //Assert
        }


        public List<LenderModel> GetLendersList()
        {
            LenderDataSource objLender = new LenderDataSource("MarketData.csv");
            return objLender.GetAllEntities();
        }
    }
}
