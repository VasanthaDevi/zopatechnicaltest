﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zopa.Models;
using Zopa.Interfaces;
using System.Configuration;


namespace Zopa.Services
{
    public class RateCalculation : IRateCalculation
    {
        private readonly int LendingPeriodInMonths = 36;
        public readonly int LoanAmountMinValue = 1000;
        public readonly int LoanAmountMaxValue = 15000;
        public readonly int LoanAmountStep = 100;

        public QuoteResultModel CalculateRate(int requested, IEnumerable<LenderModel> lenders)
        {

            var loanAmount = ValidateLoanAmount(requested);

            IEnumerable<OfferModel> offersAssigned = SelectOfferesWithLowestIntrestRates(requested, lenders);
            if (!requested.Equals(offersAssigned.Sum(offer => offer.AmountAssigned)))
            {
                throw new Exception("Market does not have sufficient offers from lenders to satisfy the loan, Requested Loan: {0}");
            }

            decimal totalRepayment = offersAssigned.Sum(offer => CalculateTotalLoanForOffer(offer));
            decimal annualRate = offersAssigned.Sum(offer => (offer.AmountAssigned / (decimal)requested) * offer.Lender.Rate);

            QuoteResultModel quoteResult = new QuoteResultModel()
            {
                LoanAmount = requested,
                TotalRepayment = totalRepayment,
                MonthlyPayment = totalRepayment / LendingPeriodInMonths,
                AnnualRate = annualRate
            };

            return quoteResult;
        }


        public  int ValidateLoanAmount(int loanAmount)
        {
             if (loanAmount < LoanAmountMinValue || loanAmount > LoanAmountMaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(loanAmount), loanAmount, $"Loan amount must be inside the interval of {LoanAmountMinValue} to {LoanAmountMaxValue}.");
            }

            if (loanAmount % LoanAmountStep != 0)
            {
                throw new ArgumentException($"Loan amount must be dividable by {LoanAmountStep}.", nameof(loanAmount));
            }

            return loanAmount;
        }

      
        public IEnumerable<OfferModel> SelectOfferesWithLowestIntrestRates(int loamAmount,
            IEnumerable<LenderModel> lenders)
        {
            int totalAmountAssigned = 0;
            lenders = lenders.OrderBy(lender => lender.Rate);

            foreach (LenderModel lender in lenders)
            {
                if (totalAmountAssigned.Equals(loamAmount)) yield break;

                OfferModel offer = new OfferModel()
                {
                    AmountAssigned = (int)Math.Min(lender.Available, loamAmount - totalAmountAssigned),
                    Lender = lender
                };
                totalAmountAssigned += offer.AmountAssigned;    
                yield return offer;
            }
        }

       //Loan amortization formula, more details: https://en.wikipedia.org/wiki/Amortization_calculator
        private decimal CalculateTotalLoanForOffer(OfferModel offer)
        {
            decimal totalLoan = 0.0m,
                loanInitialValue = offer.AmountAssigned,
                monthlyLoanRate = offer.Lender.Rate / 12,
                totalNumberOfPayments = LendingPeriodInMonths,
                comboundIntrestRate = Convert.ToDecimal(Math.Pow(1 + (double)monthlyLoanRate, (double)totalNumberOfPayments));

            totalLoan = (monthlyLoanRate * loanInitialValue * comboundIntrestRate) / (comboundIntrestRate - 1);

            return totalLoan * totalNumberOfPayments;
        }
    }
}