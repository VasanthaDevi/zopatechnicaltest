﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Zopa.Models;
using Zopa.Interfaces;

namespace Zopa.Services
{
    public class LenderDataSource : IDataSource<LenderModel>
    {
        private readonly string csvFilePath;

        public LenderDataSource(string csvFilePath)
        {
            this.csvFilePath = csvFilePath;
        }

        public List<LenderModel> GetAllEntities()
        {
            var csv = new CsvReader(new StreamReader(csvFilePath));
            var lenders = csv.GetRecords<LenderModel>();

            return lenders.ToList();
        }

    }
}